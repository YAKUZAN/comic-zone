<html>
    <head>
        <title>Forgot Password</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="main.css">
        <script src="passwordCheck.js"></script>
        

    </head>
    <body>
        <?php
             include'header.php';
        ?>
        <h1 class="comicFont text-center">Forgot your password?</h1>
        <p class="text-center">Don't worry! You can make a new one!</p>
        <form action="changePass.php" method="POST" onSubmit="return passConfirm()">
            <div class="form-row">
                <div class="col-5"></div>
                <div class="col-lg-2 col-12">
                    <label>Email</label>
                  <input type="email" name="email" class="form-control" placeholder="Enter your email" required>
                    <label>New Password</label>
                    <input type="password" id="psd" name="pass" class="form-control" placeholder="Password" required>
                     <label>Confirm Password</label>
                    <input type="password" id="cpsd" name="cpass" class="form-control" placeholder="Confirm Password" required>
                    <button type="submit" class="btn btn-block btn-dark">Make new password</button>

                </div>
                
            </div>
        </form>