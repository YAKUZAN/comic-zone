<?php
    session_start();
?>
<html>
    <head>
        <title>Comic Zone</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="main.css">

    </head>
    <body >
        
        <?php
            if(isset($_SESSION["uname"])){
                include 'loggedHeader.php';
            }
            else{
                include 'header.php';
            }
            if(!(isset($_SESSION["Cart"]))){
                    $_SESSION['Cart'] = array();
                }

        
        ?>
        
        <h1 class="comicFont text-center">Newest Stock!</h1>
      
            <img class="col-lg-2 col-4" src="https://cdn.europosters.eu/image/750/posters/batman-comic-i4516.jpg">
            <img class="col-lg-2 col-4" src="https://images-na.ssl-images-amazon.com/images/S/cmx-images-prod/Item/425423/425423._SX360_QL80_TTD_.jpg">
            <img class="col-lg-2 col-4" src="https://d1466nnw0ex81e.cloudfront.net/n_iv/600/1058637.jpg">
            <img class="col-lg-2 col-4" src="https://images-na.ssl-images-amazon.com/images/S/cmx-images-prod/Item/263932/DIG063347_1._SX360_QL80_TTD_.jpg">

            <img class="col-lg-2 col-4" src="https://cdn.shopify.com/s/files/1/0030/3802/products/51136.jpg?v=1475082392">

        
    </body>
</html>