<html>
    <head>
        <title>Register</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
        <script src="passwordCheck.js"></script>
        <link rel="stylesheet" type="text/css" href="main.css">
        
        
    </head>
    <body>
        <?php
             include'header.php';
        ?>
        <h1 class="comicFont text-center">Register</h1>
        <form action="registration.php" method="POST" onSubmit="return passConfirm()">
            <div class="form-row">
                <div class="col-4"></div>
                <div class="col-lg-2 col-sm-4">
                    <label>Email address</label>
                  <input type="email" class="form-control" placeholder="Enter Email" name="email" required>
                </div>
                <div class="col-lg-2 col-sm-4">
                    <label>Username</label>
                  <input type="text" class="form-control" placeholder="Enter Username" name="uname" required>
                </div>
                <div class="col-4"></div> 
              </div>
            <div class="form-row">
                <div class="col-4"></div>
                <div class="col-lg-2 col-sm-4">
                    <label>Password</label>
                  <input type="password" class="form-control" id="psd"placeholder="Password" name="psd" required>
                </div>
                <div class="col-lg-2 col-sm-4">
                    <label>Confirm Password</label>
                  <input type="password" class="form-control"id="cpsd" placeholder="Confirm Password" name="cpsd" required>
                </div>
                <div class="col-4"></div> 
              </div>
                <br>
              <div class="form-row">
                <div class="col-4"></div>
                <div class="col-4">
                    <input type="submit" value="Register"class="btn btn-dark btn-lg btn-block">

                </div>
              </div>
        </form>
    </body>
</html>