<header>
            <img id="headerImage" src="432931.jpg">
            <nav class="navbar sticky-top navbar-expand-lg navbar-dark bg-dark">
              <a class="navbar-brand comicFont" href="index.php">Comic Zone</a>
              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>

              <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
               
                     <li class="nav-item">
                    <a class="nav-link" href="advancedSearch.php">Advanced Search</a>
                  </li> 
                <li class="nav-item">
                    <a class="nav-link" href="shoppingCart.php">Shopping Cart</a>
                  </li> 
                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                    Account Settings
                  </a>
                  <div class="dropdown-menu">
                    <a class="dropdown-item" href="purchases.php">Previous Purchases</a>
                    <a class="dropdown-item" href="changeemail.php">Change email</a>
                    <a class="dropdown-item" href="loggingOut.php">Log out</a>
                  </div>
                </li>
                </ul>

                <form method="post" action="searchResults.php" class="form-inline my-2 my-lg-0">
                  <input class="form-control mr-sm-2" name="search"type="search" placeholder="Search comic" aria-label="Search">
                  <button class="btn my-2 my-sm-0" type="submit">Search</button>
                </form>
              </div>
            </nav>
        </header>