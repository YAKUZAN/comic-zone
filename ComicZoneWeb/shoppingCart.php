<?php
    session_start();
    require_once("dbConnect.php");
    if(empty($_SESSION['Cart'])){
        echo "<SCRIPT type='text/javascript'>
            alert('Shopping Cart Empty');
            window.location.replace('index.php');
            </SCRIPT>";
    }
    else{
    
?>
    <html>
    <head>
        <title>Shopping Cart</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="main.css">

    </head>
    <body >
        <?php
            include 'loggedHeader.php';
        ?>
         <h1 class="comicFont text-center">Shopping Cart</h1>
        <?php
            $totalPrice=0;
        foreach ($_SESSION['Cart'] as $key => $value) {
            $sql="SELECT * FROM comic WHERE ComicId ='$value'"; 
            $result=mysqli_query($conn,$sql)
                or die("Error in query:".mysqli_error($conn));
            if ($row=mysqli_fetch_assoc($result)){
                $ComicName=$row["ComicName"];
                $Price=$row["Price"];
                $Image=$row["Image"];
                $totalPrice=$totalPrice+$Price;
                echo"<div class='col-lg-2 col-5'><img class='searchImage'src='".$Image."'>"."<br>".$ComicName."<br>€".$Price."</div>";
            
        }
        }
        echo 
            "<form action='DCheckout.php'method='POST'class='text-center'>
            <p>Total: €".$totalPrice. "</p><br>
            <label>Discount Code</label>
                
                  <input type='text' name='discountCode' class='form-control' placeholder='Code'>
                  <br><button class='btn btn-dark' name='price'value='".$totalPrice."'type='submit'>Checkout with discount code</button>
                  </form>
                  <form action='noDCheckout.php' method='POST' class='text-center'>
      <button class='btn btn-dark' name='price'value='".$totalPrice."'type='submit'>Checkout without discount code</button></div><br>
      </form>";
        mysqli_close($conn);
    }
?>