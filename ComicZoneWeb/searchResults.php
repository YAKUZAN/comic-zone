<?php
    session_start();
    require_once("dbConnect.php");
    $search=$_POST['search'];
?>
<html>
    <head>
        <title>Search Results</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="main.css">

    </head>
    <body>
        
        <?php
            if(isset($_SESSION["uname"])){
                include 'loggedHeader.php';
                echo"<h1 class='comicFont text-center'>Search Results</h1>
                <form class='container'action='addToCart.php' method='POST'>";
            }
            else{
                include 'header.php';
                echo"<h1 class='comicFont text-center'>Search Results</h1>
                <form action='login.php' method='POST'>";
            }
            
            $sql="SELECT * FROM comic WHERE ComicName LIKE '%$search%'
            AND Stock>0";
            $result=mysqli_query($conn,$sql)
                or die("Error in query:".mysqli_error($conn));
            ?>
            <div class='row container'>
            
            <?php
        while ($row = mysqli_fetch_assoc($result)){
                $ComicName=$row["ComicName"];
                $ComicId=$row["ComicId"];
                $Price=$row["Price"];
                $Image=$row["Image"];
                
            if(isset($_SESSION["uname"])){
                echo "<div class='col-lg-2  col-5'><img class='searchImage'src='".$Image."'>"."<br>".$ComicName."<br>€".$Price."<br><button class='btn btn-dark' type='submit' value='$ComicId' name='CartAdd'>Add to Cart!</button></div>";
            }
            else{
                echo "<div class='col-lg-2 col-5'><img class='searchImage'src='".$Image."'>"."<br>".$ComicName."<br>€".$Price."<br><button class='btn btn-dark' type='submit'>Log in first</button></div>";
       
            }
        }

        ?>
        </form>
        </div>