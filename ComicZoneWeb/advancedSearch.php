<?php
    session_start();
?>
<html>
    <head>
        <title>Advanced Search</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="main.css">

    </head>
    <body >
        <?php
            if(isset($_SESSION["uname"])){
                include 'loggedHeader.php';
            }
            else{
                include 'header.php';
            }
        
        ?>
        <br>
        <h1 class="ComicFont text-center">Advanced Search</h1>
          <form action="advancedSearchResults.php" method="POST" class="container">
            <div class="form-row">
                <div class="col-4"></div>
                <div class="col-lg-4">
                     <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Publisher</label>
                      <select class="custom-select my-1 mr-sm-2" id="inlineFormCustomSelectPref" name="publisher">
                        <?php   
                        require_once("dbConnect.php");
                        $sql="SELECT * FROM publisher";
                        $result=mysqli_query($conn,$sql)
                            or die("Error in query:".mysqli_error($conn));
                        while ($row = mysqli_fetch_assoc($result)) {
                                echo '<option value="'.$row[PublisherId].'">'.$row[PublisherName].'</option>';
                        }
                            ?>
                      </select>
                </div>
              </div>
            <div class="form-row">
                <div class="col-4"></div>
                <div class="col-lg-4">
                     <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Category</label>
                      <select class="custom-select my-1 mr-sm-2" id="inlineFormCustomSelectPref" name="category">
                        <?php
                        $sql2="SELECT * FROM category";
                        $result2=mysqli_query($conn,$sql2)
                            or die("Error in query:".mysqli_error($conn));
                        while ($row = mysqli_fetch_assoc($result2)) {
                                echo '<option value="'.$row[CategoryId].'">'.$row[CategoryName].'</option>';
                        }
                            ?>
                      </select>
                </div>
              </div>    
              <div class="form-row">
                <div class="col-4"></div>
                
                <div class="col-lg-4">
                    <label>Price</label>
                    <input type="text" class="form-control" placeholder="Enter price" name="price" required>
                </div>
              </div>
              <br>
              <div class="form-row">
                <div class="col-4"></div>
                
                <div class="col-4">
                    <input type="submit" value="Search"class="btn btn-dark btn-lg btn-block">
                </div>
              </div>
              
        </form>
    
        
    </body>
</html>