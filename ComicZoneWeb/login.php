<html>
    <head>
        <title>Log in</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="main.css">
        <link rel="stylesheet" type="text/css" href="login.css">
        

    </head>
    <body>
       <?php
            include'header.php';
        ?>
        <h1 class="comicFont text-center">Log in</h1>
        <form action="loggingIn.php" method="POST">
            <div class="form-row">
                <div class="col-5"></div>
                <div class="col-lg-2">
                    <label>Username</label>
                  <input type="text" name="uname" class="form-control" placeholder="Username" required>
                    <label>Password</label>
                  <input type="password" name="psd" class="form-control" placeholder="Password" required>
                    <br>
                    <button type="submit" class="btn btn-block btn-dark">Log in</button>
                    <a href="forgotpass.php"><button type="button" class="btn btn-block btn-link">Forgot Password?</button></a>
                </div>
                
            </div>
        </form>
    </body>
</html>