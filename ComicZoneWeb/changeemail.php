    <html>
    <head>
        <title>Comic Zone</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="main.css">
        <link rel="stylesheet" type="text/css" href="login.css">
        

    </head>
    <body>
       <?php
            include'loggedHeader.php';
        ?>
          <h1 class="comicFont text-center">Change Email</h1>
        <form action="newEmail.php" method="POST">
            <div class="form-row">
                <div class="col-5"></div>
                <div class="col-lg-2 col-12">
                    <label>New Email</label>
                  <input type="email" name="email" class="form-control" placeholder="Enter your email" required>
                    <button type="submit" class="btn btn-block btn-dark">Change Email</button>

                </div>
                
            </div>
        </form>
    </body>
</html>