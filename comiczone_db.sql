-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 25, 2018 at 12:32 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `comiczone_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `CategoryId` int(11) NOT NULL,
  `CategoryName` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`CategoryId`, `CategoryName`) VALUES
(1, 'Romance'),
(3, 'Action');

-- --------------------------------------------------------

--
-- Table structure for table `comic`
--

CREATE TABLE `comic` (
  `ComicId` int(11) NOT NULL,
  `ComicName` varchar(30) DEFAULT NULL,
  `Price` float DEFAULT NULL,
  `Stock` int(11) DEFAULT NULL,
  `NumberOfPurchases` int(11) DEFAULT NULL,
  `Likes` int(11) DEFAULT NULL,
  `Dislikes` int(11) DEFAULT NULL,
  `CategoryId` int(11) DEFAULT NULL,
  `PublisherId` int(11) DEFAULT NULL,
  `Image` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comic`
--

INSERT INTO `comic` (`ComicId`, `ComicName`, `Price`, `Stock`, `NumberOfPurchases`, `Likes`, `Dislikes`, `CategoryId`, `PublisherId`, `Image`) VALUES
(6, 'Batman', 5.5, 96, 54, 30, 20, 3, 2, 'https://cdn.europosters.eu/image/750/posters/batman-comic-i4516.jpg'),
(7, 'Spiderman', 6.5, 14, 22, 2, 0, 3, 1, 'https://images-na.ssl-images-amazon.com/images/S/cmx-images-prod/Item/425423/425423._SX360_QL80_TTD_.jpg'),
(16, 'Spiderman 2', 5, 4, 10, 5, 1, 3, 1, 'https://d1466nnw0ex81e.cloudfront.net/n_iv/600/1058637.jpg'),
(17, 'Wonder Woman', 10, 50, 0, 0, 0, 3, 2, 'https://images-na.ssl-images-amazon.com/images/S/cmx-images-prod/Item/263932/DIG063347_1._SX360_QL80_TTD_.jpg'),
(20, 'Deadpool', 4.5, 20, 0, 0, 0, 3, 1, 'https://cdn.shopify.com/s/files/1/0030/3802/products/51136.jpg?v=1475082392');

-- --------------------------------------------------------

--
-- Table structure for table `discounts`
--

CREATE TABLE `discounts` (
  `CodeId` int(11) NOT NULL,
  `Code` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `discounts`
--

INSERT INTO `discounts` (`CodeId`, `Code`) VALUES
(2, '2EUROOFF');

-- --------------------------------------------------------

--
-- Table structure for table `publisher`
--

CREATE TABLE `publisher` (
  `PublisherId` int(11) NOT NULL,
  `PublisherName` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `publisher`
--

INSERT INTO `publisher` (`PublisherId`, `PublisherName`) VALUES
(1, 'Marvel'),
(2, 'DC');

-- --------------------------------------------------------

--
-- Table structure for table `purchasecomic`
--

CREATE TABLE `purchasecomic` (
  `PurchaseId` int(11) NOT NULL,
  `ComicId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `purchases`
--

CREATE TABLE `purchases` (
  `PurchaseId` int(11) NOT NULL,
  `PurchaseDate` date NOT NULL,
  `TotalPrice` int(11) NOT NULL,
  `UserId` int(11) NOT NULL,
  `CodeId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `UserId` int(11) NOT NULL,
  `UserName` varchar(30) NOT NULL,
  `Password` varchar(30) NOT NULL,
  `Email` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`UserId`, `UserName`, `Password`, `Email`) VALUES
(1, 'test', 'test', 'test@gmail.com');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`CategoryId`);

--
-- Indexes for table `comic`
--
ALTER TABLE `comic`
  ADD PRIMARY KEY (`ComicId`),
  ADD KEY `CategoryId` (`CategoryId`),
  ADD KEY `PublisherId` (`PublisherId`);

--
-- Indexes for table `discounts`
--
ALTER TABLE `discounts`
  ADD PRIMARY KEY (`CodeId`);

--
-- Indexes for table `publisher`
--
ALTER TABLE `publisher`
  ADD PRIMARY KEY (`PublisherId`);

--
-- Indexes for table `purchasecomic`
--
ALTER TABLE `purchasecomic`
  ADD PRIMARY KEY (`PurchaseId`,`ComicId`),
  ADD KEY `PurchaseId` (`PurchaseId`),
  ADD KEY `ComicId` (`ComicId`);

--
-- Indexes for table `purchases`
--
ALTER TABLE `purchases`
  ADD PRIMARY KEY (`PurchaseId`),
  ADD KEY `CodeId` (`CodeId`),
  ADD KEY `UserId` (`UserId`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`UserId`),
  ADD UNIQUE KEY `UserName` (`UserName`,`Email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `CategoryId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `comic`
--
ALTER TABLE `comic`
  MODIFY `ComicId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `discounts`
--
ALTER TABLE `discounts`
  MODIFY `CodeId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `publisher`
--
ALTER TABLE `publisher`
  MODIFY `PublisherId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `purchases`
--
ALTER TABLE `purchases`
  MODIFY `PurchaseId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `UserId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `comic`
--
ALTER TABLE `comic`
  ADD CONSTRAINT `comic_ibfk_1` FOREIGN KEY (`CategoryId`) REFERENCES `category` (`CategoryId`),
  ADD CONSTRAINT `comic_ibfk_2` FOREIGN KEY (`PublisherId`) REFERENCES `publisher` (`PublisherId`);

--
-- Constraints for table `purchasecomic`
--
ALTER TABLE `purchasecomic`
  ADD CONSTRAINT `purchasecomic_ibfk_1` FOREIGN KEY (`PurchaseId`) REFERENCES `purchases` (`PurchaseId`),
  ADD CONSTRAINT `purchasecomic_ibfk_2` FOREIGN KEY (`ComicId`) REFERENCES `comic` (`ComicId`);

--
-- Constraints for table `purchases`
--
ALTER TABLE `purchases`
  ADD CONSTRAINT `purchases_ibfk_1` FOREIGN KEY (`CodeId`) REFERENCES `discounts` (`CodeId`),
  ADD CONSTRAINT `purchases_ibfk_2` FOREIGN KEY (`UserId`) REFERENCES `user` (`UserId`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
